provider "aws" {
  region = "us-east-1"  # Update with your desired region
}

resource "aws_instance" "example" {
  ami           = "ami-0440d3b780d96b29d"
  instance_type = "t2.micro"
  key_name = "main"

  provisioner "local-exec" {
    command = "puppet apply /home/master/puppet/site.pp"
  }
}

